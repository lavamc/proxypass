# ProxyPass

### Introduction

Proxy pass allows developers to MITM a vanilla client and server without modifying them. This allows for easy testing 
of the Bedrock Edition protocol and observing vanilla network behavior.

This version of ProxyPass has been modified for the [LavaMC](https://gitlab.com/lavamc) project.

__ProxyPass requires Java 8 u162 or later to function correctly due to the encryption used during login__

### Links

__[Jenkins](https://ci.nukkitx.com/job/NukkitX/job/ProxyPass/job/master/) (does not include LavaMC changes)__

__[Protocol library](https://github.com/NukkitX/Protocol) used in this project__
