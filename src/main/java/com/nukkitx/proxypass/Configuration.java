package com.nukkitx.proxypass;

import lombok.Getter;
import lombok.ToString;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.Set;

@Getter
@ToString
public class Configuration {
    private Address proxy = new Address();
    private Address destination = new Address();

    private boolean passingThrough = true;

    public Configuration(String proxyHost, int proxyPort, String destinationHost, int destinationPort) {
        proxy.host = proxyHost;
        proxy.port = proxyPort;
        destination.host = destinationHost;
        destination.port = destinationPort;
    }

    @Getter
    @ToString
    public static class Address {
        private String host;
        private int port;

        InetSocketAddress getAddress() {
            return new InetSocketAddress(host, port);
        }
    }
}
